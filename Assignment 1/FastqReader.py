import argparse as ap
from multiprocessing import Process, Queue
import os
import csv


def finalyze(combinedlist):
    total = []
    for avaragelines in combinedlist:
        for position in range(len(avaragelines)):
            try:
                total[position] = total[position] + avaragelines[position]
            except:
                total.append(avaragelines[position])

    endresutls = []
    for score in total:
        endresutls.append(score/len(combinedlist))
    del total
    return endresutls


def fileout(final_scores, outfile):
    if not os.path.exists("output/"):
        os.mkdir("output/")
    else:
        pass

    with open("output/{}".format(outfile), 'w') as output:
        writer = csv.writer(output)
        writer.writerows([["Position", "Average Score"]])
        for i, score in enumerate(final_scores):
            writer.writerow([str(i), score])
    output.close()


def mean_calculator(list_of_scores, q):
    means = []
    for position in list_of_scores:
        means.append(sum(position)/len(position))
    q.put(means)


def file_slicer(q, startbytes, chunk_size, file):
    with open(file) as fastq:
        fastq.seek(startbytes)
        fastq.readline()
        chunk_start_check = False
        potential_header = False
        list_of_scores = []
        for i, line in enumerate(fastq.readlines(chunk_size)):
            line = line.strip("\n")
            if not chunk_start_check:
                if potential_header:
                    if line.startswith("@"):
                        first_headerline = i
                        chunk_start_check = True
                    else:
                        chunk_start_check = True
                if line.startswith("@") and not potential_header:
                    first_headerline = i
                    potential_header = True

            if chunk_start_check:
                if (i+first_headerline) % 4 == 3:
                    for score in range(len(line)):
                        try:
                            list_of_scores[score].append(int(ord(line[score]) - 33))
                        except:
                            list_of_scores.append([(int(ord(line[score]) - 33))])

        mean_calculator(list_of_scores, q)
        del list_of_scores
    del fastq


def jobs(args):
    proceslist = []
    q = Queue()
    file_size = os.stat(args.fastq[0]).st_size
    chunk_size = int(file_size / args.cores)
    chunk_index = 0
    for i in range(0, args.cores):
        print("processing file chunk {}".format(chunk_index))
        p = Process(target=file_slicer, args=(q, chunk_size*i, chunk_size, args.fastq[0]))
        p.start()
        proceslist.append(p)
        chunk_index += 1

    # Prints the results off all processes and terminates these processes
    combinedlist = []
    for x in proceslist:
        x.join()
        combinedlist.append(q.get())
    if q.empty():
        if args.csv:
            fileout(finalyze(combinedlist), args.csv)
        else:
            print(finalyze(combinedlist))

def main(args):
    parser = ap.ArgumentParser(description='Process FastQ file to produce average PHRED scores per base')
    parser.add_argument('fastq', nargs=1)
    parser.add_argument('-n', dest='cores', type=int, required=True)
    parser.add_argument('-o', dest='csv'),
    args = parser.parse_args()
    jobs(args)


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
