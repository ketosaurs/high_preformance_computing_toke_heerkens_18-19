import argparse as ap
from multiprocessing import Process, Queue, managers
import os
import csv
import subprocess
import random
import time

ssh_adresses = ["bin204", "bin205", "bin206", "bin207"]
ports = range(15000, 20000)

portnummer = random.choice(ports)
key = b'secretkey'


def finalyze(combinedlist):
    '''
    takes all the resultes lists and calculates the total avrages

    :param combinedlist:
    :return single list of the avrages socre per position:
    '''
    total = []
    for avaragelines in combinedlist:
        for position in range(len(avaragelines)):
            try:
                total[position] = total[position] + avaragelines[position]
            except:
                total.append(avaragelines[position])

    endresutls = []
    for score in total:
        endresutls.append(score/len(combinedlist))
    del total
    return endresutls


def fileout(final_scores, outfile):
    '''
    writhes the results to a csv file.

    :param final_scores:
    :param outfile:
    :return:
    '''
    if not os.path.exists("output/"):
        os.mkdir("output/")
    else:
        pass

    with open("output/{}".format(outfile), 'w') as output:
        writer = csv.writer(output)
        writer.writerows([["Position", "Average Score"]])
        for i, score in enumerate(final_scores):
            writer.writerow([str(i), score])
    output.close()


def mean_calculator(list_of_scores):
    '''
    calculates the means of a file chunk

    :param list_of_scores:
    :param q:

    puts the calculated means into the queue
    '''
    means = []
    for position in list_of_scores:
        means.append(sum(position)/len(position))
    return means


def chunk_procesor(file, job_q, result_q):
    calculating = False
    chunk_size = int(os.stat(file).st_size/1000)
    while not job_q.empty():
        with open(file) as fastq:
            try:
                if not calculating and not job_q.empty():
                    try:
                        startbyte = job_q.get()
                    except EOFError:
                        return
                    fastq.seek(startbyte)
                    fastq.readline()
                    calculating = True
                else:
                    return
                while calculating:
                    chunk_start_check = False
                    potential_header = False
                    list_of_scores = []
                    for i, line in enumerate(fastq.readlines(chunk_size)):
                        line = line.strip("\n")
                        if not chunk_start_check:
                            if potential_header:
                                if line.startswith("@"):
                                    first_headerline = i
                                    chunk_start_check = True
                                else:
                                    chunk_start_check = True
                            if line.startswith("@") and not potential_header:
                                first_headerline = i
                                potential_header = True

                        if chunk_start_check:
                            if (i + first_headerline) % 4 == 3:
                                for score in range(len(line)):
                                    try:
                                        list_of_scores[score].append(int(ord(line[score]) - 33))
                                    except:
                                        list_of_scores.append([(int(ord(line[score]) - 33))])

                    result_q.put(mean_calculator(list_of_scores))
                    calculating = False
                    del list_of_scores
            except job_q.empty():

                return
    return

def start_master(args):
    master_machien = server_master(portnummer, key)
    shared_work_queue = master_machien.get_job_q()
    shared_results_queue = master_machien.get_result_q()
    chunks = chunk_generator(os.stat(args.fastq[0]).st_size)
    for chunk in chunks:
        shared_work_queue.put(chunk)

    machien_booter(args)

    total_scorelist = []
    number_of_received_results = 0
    while number_of_received_results <= len(chunks):
        print(number_of_received_results)
        total_scorelist.append(shared_results_queue.get())
        number_of_received_results += 1
        if number_of_received_results == len(chunks):
            break
    if args.csv:
        fileout(finalyze(total_scorelist), args.csv)
    else:
        print(finalyze(total_scorelist))


def machien_booter(args):
    for computer in ssh_adresses:
        argument = "{} -n {} -m client -bin {} -port {}".format(args.fastq[0], args.cores, computer, portnummer)
        subprocess.Popen(["ssh", computer, "python3", "~/Leerjaar_3/Thema_12/HPC/FastqReaderManagers.py", argument])
    return


def start_client(file, cores, binnummer, port, key):
    client = client_creator(binnummer, port, key)
    job_q = client.get_job_q()
    result_q = client.get_result_q()

    generat_processes(file, cores, job_q, result_q)

def generat_processes(file, cores, job_q, result_q):
    list_of_processes = []
    p = Process(target=chunk_procesor, args=(file, job_q, result_q))
    list_of_processes.append(p)
    p.start()
    p.join()


def chunk_generator(filesize):
    TotalNumberOfChunks = 1000
    filechunksize = int(filesize/TotalNumberOfChunks)
    chunk_list = [filechunksize*i for i in range(0, TotalNumberOfChunks)]
    return chunk_list


def server_master(port, authkey):
    """ Create a manager for the server, listening on the given port.
        Return a manager object with get_job_q and get_result_q methods.
    """
    job_q = Queue()
    result_q = Queue()


    # This is based on the examples in the official docs of multiprocessing.
    # get_{job|result}_q return synchronized proxies for the actual Queue
    # objects.
    class JobQueueManager(managers.SyncManager):
        pass

    JobQueueManager.register('get_job_q', callable=lambda: job_q)
    JobQueueManager.register('get_result_q', callable=lambda: result_q)

    manager = JobQueueManager(address=('bin204', port), authkey=authkey)
    manager.start()
    print('Server started at port %s' % port)
    return manager


def client_creator(ip, port, authkey):
    """ Create a manager for a client. This manager connects to a server on the
        given address and exposes the get_job_q and get_result_q methods for
        accessing the shared queues from the server.
        Return a manager object.
    """
    class ServerQueueManager(managers.SyncManager):
        pass

    ServerQueueManager.register('get_job_q')
    ServerQueueManager.register('get_result_q')
    manager = ServerQueueManager(address=(ip, int(port)), authkey=key)

    connection_attempts = 0
    while True:
        try:
            if connection_attempts == 5:
                sys.exit(1)
            manager.connect()
            break
        except ConnectionRefusedError:
            time.sleep(3)
            connection_attempts += 1
    return manager


def main(args):
    parser = ap.ArgumentParser(description='Process FastQ file to produce average PHRED scores per base')
    parser.add_argument('fastq', nargs=1)
    parser.add_argument('-n', dest='cores', type=int, required=True)
    parser.add_argument('-out', dest='csv'),
    parser.add_argument('-m', dest='manager')
    parser.add_argument("-bin", dest="binnumber")
    parser.add_argument("-port", dest="port")
    parser.add_argument("-key", dest="key")
    args = parser.parse_args()
    #listOfChunks = chunk_generator(os.stat(args.fastq[0]).st_size)
    if args.manager == "server":
        start_master(args)
    if args.manager == "client":
        start_client(args.fastq[0], args.cores, args.binnumber, args.port, args.key)



if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
